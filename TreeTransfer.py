#!/usr/bin/env python3

"""
TreeTransfer - A lightweight program for transferring files over networks
Copyright (C) 2017  Trevor Dumas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import socket
import os

try:
    import ssl
    SSL_PRESENT = True
except ImportError:
    SSL_PRESENT = False

def getTree(fullpath):
    """Function that returns a list of all files and directories contained in a given path.\
    The list that is returned contains tuples that each contain two elements: (Absolute path, Path relative to common path)."""
    files = []
    fullpath = os.path.abspath(fullpath)
    if os.path.isfile(fullpath): # The specified path is a filename
        files.append((fullpath, os.path.basename(fullpath)))
        return files
    elif os.path.isdir(fullpath): # The specified path is a directory
        for folderdata in os.walk(fullpath):
            files.append(os.path.abspath(folderdata[0]))
            for subfile in folderdata[2]:
                files.append(os.path.abspath(os.path.join(folderdata[0], subfile)))
        prefix = os.path.commonpath(files)
        if prefix == os.sep: # Common directory is root dir
            for i in range(len(files)):
                files[i] = (files[i], os.path.join("SYSTEMROOT", files[i][len(prefix):])) # Use SYSTEMROOT as common path so all files are placed in one directory
        else: # Common directory is not root dir
            newprefix = os.path.basename(prefix.rstrip(os.sep)) # Remove right separator from common path and set newprefix to the last folder name (newprefix represents)
            for i in range(len(files)):
                files[i] = (files[i], os.path.join(newprefix, files[i][len(prefix):][len(os.sep):])) # Remove all but the last common folder in the file's path
        return files
    else: # The specified path does not exist or is not readable
        raise IOError("File or directory does not exist or is not readable.")


def sendTree(fullpath, port, srv_key, srv_cert):
    """Function that sends a directory and its contents to a client."""
    tree = getTree(fullpath) # Get the tree of files to send
    srv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create a socket and set appropriate options
    if SSL_PRESENT:
        print("SSL is available.")
    else:
        print("SSL is unavailable.")
    if srv_key != "" and srv_cert != "" and SSL_PRESENT: # Prepares the socket for SSL if available
        print("Using SSL.")
        try:
            context = ssl.SSLContext(ssl.PROTOCOL_TLS)
            context.load_cert_chain(srv_cert, srv_key)
            srv_sock = context.wrap_socket(srv_sock, server_side = True, do_handshake_on_connect = True)
        except:
            print("Could not set up an SSL socket with the given certificate and key.")
            raise Exception
    else:
        print("Not using SSL.")
    srv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    srv_sock.bind(("", port))
    srv_sock.listen(1)
    client_sock, client_addr = srv_sock.accept() # Accept a client connection
    print("Connection from: ", client_addr)
    for i in range(len(tree)):
        if os.path.isdir(tree[i][0]): # Path represents a directory
            path_length = str(len(tree[i][1]))
            while len(path_length) < 16:
                path_length = "0" + path_length
            path_length = bytes(path_length, "UTF8")
            client_sock.send(b"0" + path_length + bytes(tree[i][1], "UTF8")) # Send bytes in the format: 0 (indicating directory) + 16 digits representing length of pathname + bytes representing the pathname
        elif os.path.isfile(tree[i][0]): # Path represents a file
            try:
                fsize = os.path.getsize(tree[i][0]) # Try to get file size
                fsize = str(fsize)
                while len(fsize) < 16:
                    fsize = "0" + fsize
                fsize = bytes(fsize, "UTF8")
                filehandle = open(tree[i][0], "rb")
            except (OSError, PermissionError, FileNotFoundError): # If the file cannot be read, inform the user and do not send file
                print("Error: file ", tree[i][0], " cannot be sent.")
                client_sock.send(b"2") # Send byte "2" to client indicating that a file cannot be sent.
                continue # Skip file and move on to next element
            fnamelength = str(len(tree[i][1])) # Get relative filename for client
            while len(fnamelength) < 16: # Put length of filename in 16 byte format
                fnamelength = "0" + fnamelength
            fnamelength = bytes(fnamelength, "UTF8")
            fname = bytes(tree[i][1], "UTF8")
            client_sock.send(b"1" + fsize + fnamelength + fname) # Send bytes in the format: 1 (indicating file) + 16 digits representing file size + 16 digits representing filename length + bytes representing filename
            fname = fname.decode("UTF8") # Convert fname back to string for later use
            fsize = int(fsize) # Convert fsize back to usable form
            bytes_sent = 0
            wrong_fsize = False # Flag variable for incorrect file size (in the event that a file is modified before it is sent)
            while bytes_sent < fsize: # Until all bytes have been sent
                if bytes_sent <= fsize - 4096: # 4096 or more bytes remaining to send
                    data_sending = filehandle.read(4096)
                    client_sock.send(data_sending) # Send 4096 bytes from file
                    bytes_sent += 4096
                else: # Less than 4096 bytes left to send
                    data_sending = filehandle.read(fsize - bytes_sent)
                    client_sock.send(data_sending)
                    bytes_sent += fsize - bytes_sent
                print(fname + " (" + str(fsize) + " bytes) " + str(int(bytes_sent * 100 / fsize)) + "%" + " - DONE!" * int(bytes_sent == fsize), end=["\r", "\n"][int(bytes_sent == fsize)], flush = True) # Notify user of progress on file transfer
    client_sock.send(b"3") # Send byte "3" to indicate to the client that sending is complete.

def recvTree(sender, port, timeout, cert, savedir):
    """Function that receives a directory and its contents from the sender."""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create a socket and set appropriate options
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    if SSL_PRESENT:
        print("SSL is available.")
    else:
        print("SSL is unavailable.")
    if cert != "" and SSL_PRESENT: # Prepares the socket for SSL if available
        print("Using SSL.")
        try:
            sock = ssl.wrap_socket(sock, cert_reqs = ssl.CERT_REQUIRED, ca_certs = cert, do_handshake_on_connect = True)
        except:
            print("Could not set up an SSL socket with the given certificate.")
            raise Exception
    else:
        print("Not using SSL.")
    sock.settimeout(timeout)
    sock.connect((sender, port)) # Connect to the sender
    while True: # Loop until files are done being sent
        servercode = sock.recv(1)
        if servercode == b"3":
            print("Done receiving all files and directories.")
            break
        elif servercode == b"2":
            print("Server side error: server could not send a particular file, skipping it.")
        elif servercode == b"0":
            directorylength = b""
            while len(directorylength) < 16: # Loop until 16 byte dir length is received
                directorylength += sock.recv(16 - len(directorylength))
            directorylength = int(directorylength.decode("UTF8")) # Convert dir length to int
            directory = b""
            while len(directory) < directorylength: # Loop until entire directory has been received
                directory += sock.recv(directorylength - len(directory))
            directory = os.path.join(savedir, os.path.normpath(directory.decode("UTF8"))) # Convert path to string and normalize it according to OS path rules
            print("Making directory: ", directory)
            try:
                os.makedirs(directory) # Make the directory (may throw an exception if there is a permission issues or if illegal characters are present)
            except FileExistsError:
                pass
        elif servercode == b"1":
            fsize = b""
            while len(fsize) < 16: # Loop until 16 byte file size is received
                fsize += sock.recv(16 - len(fsize))
            fsize = int(fsize.decode("UTF8")) # Convert fsize to usable format
            fnamelength = b""
            while len(fnamelength) < 16: # Loop until 16 byte file name length is received
                fnamelength += sock.recv(16 - len(fnamelength))
            fnamelength = int(fnamelength.decode("UTF8")) # Convert fnamelength to usable format
            fname = b""
            while len(fname) < fnamelength: # Loop until entire filename is received
                fname += sock.recv(fnamelength - len(fname))
            fname = os.path.normpath(fname.decode("UTF8")) # Make fname usable and normalized
            with open(os.path.join(savedir, fname), "wb") as filehandle: # Open file in binary write mode
                bytes_read = 0
                while bytes_read < fsize: # Loop until all bytes have been received and written
                    bytes_remaining = fsize - bytes_read
                    if bytes_remaining >= 4096: # If more than 4096 or more bytes left, receive bytes in chunks of 4096
                        data = sock.recv(4096)
                    else: # If not, receive all remaining bytes
                        data = sock.recv(bytes_remaining)
                    bytes_read += len(data)
                    filehandle.write(data) # Write bytes to file
                    print(fname + " (" + str(fsize) + " bytes) " + str(int(bytes_read * 100 / fsize)) + "%" + " - DONE!" * int(bytes_read == fsize), end=["\r", "\n"][int(bytes_read == fsize)], flush = True) # Notify user of progress on file transfer

def main():
    from sys import argv
    if len(argv) < 2 or "-h" in argv or "--help" in argv:
        print("USAGE:\nTreeTransfer.py send -f [folder or filename] -p [port] -k [key] -c [certificate]")
        print("OR")
        print("TreeTransfer.py recv -s [sender's ip] -p [port] -c [certificate] -t [timeout length] -d [save directory]")
        print("NOTE: If the path/file argument has spaces, put it in quotes.")
        exit(1)
    else:
        timeout = 10
        save_dir = ""
        srv_cert = ""
        srv_key = ""
        port = 42255
        data_path = ""
        sender = "127.0.0.1"
        for i in range(len(argv)):
            if argv[i] == "-t":
                try:
                    timeout = int(argv[i + 1])
                except:
                    print("Error: could not convert the given timeout value to an integer.")
                    exit(1)
            elif argv[i] == "-d":
                save_dir = argv[i + 1]
            elif argv[i] == "-c":
                srv_cert = argv[i + 1]
                if not os.path.isfile(srv_cert):
                    print("The specified certificate does not exist.")
                    exit(1)
            elif argv[i] == "-k":
                srv_key = argv[i + 1]
                if not os.path.isfile(srv_key):
                    print("The specified key file does not exist.")
                    exit(1)
            elif argv[i] == "-p":
                try:
                    port = int(argv[i + 1])
                except:
                    print("Error: could not convert the given port to an integer.")
                    exit(1)
            elif argv[i] == "-f":
                data_path = argv[i + 1]
                if (not os.path.isdir(data_path)) and (not os.path.isfile(data_path)):
                    print("The directory or file to send does not exist.")
                    exit(1)
            elif argv[i] == "-s":
                sender = argv[i + 1]
        if argv[1].lower() == "send": # Sending a file or directory
            try:
                print("Sending: " + data_path)
                print("Port: " + str(port))
                if srv_key != "" and srv_cert != "":
                    print("Certificate: " + srv_cert)
                    print("Key: " + srv_key)
                sendTree(data_path, port, srv_key, srv_cert)
            except OSError:
                print("Error: failed to send data to the client.")
                exit(1)
        elif argv[1].lower() == "recv": # Receiving a file or directory
            try:
                print("Sender: " + sender)
                print("Port: " + str(port))
                print("Timeout: " + str(timeout) + "s")
                print("Saving in: " + save_dir)
                if srv_cert != "":
                    print("Certificate: " + srv_cert)
                recvTree(sender, port, timeout, srv_cert, save_dir)
            except OSError:
                print("Error: failed to receive data from the sender.")
                exit(1)
        else: # No valid option selected
            print("Error: \"send\" or \"recv\" must be specified.")
            exit(1)

if __name__ == "__main__":
    main()
